﻿namespace PokeAPI.Data.Entities
{
    public class Pokemon
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
